var person = {
  name: "Sorax",
  _age: 20,
  get age(){
    return this._age;
  },
  set age(value) {
    this._age = value < 0 ? 0 : value > 122 ? 122 : value;
  }
};

console.log(person.age);
person.age = 0;
console.log(person.age);

console.log(Object.getOwnPropertyDescriptor(person, "name"));
console.log(Object.getOwnPropertyDescriptor(person, "age"));

Object.defineProperty(person, "gender" , {
  value: "male",
  writable: false,
  enumerable: false,
  configurable: false
});
console.log(person.gender);
person.gender = "famale";
console.log(person.gender);

for(property in person) {
  console.log(property);
}

console.log(Object.keys(person));
console.log(person.propertyIsEnumerable("gender"));

var o = {};
Object.defineProperties(o, {
  x: {
    value: 10,
    writable: false
  },
  y: {
    value: 20,
    writable: false
  },
  r: {
    get: function() {
      return Math.sqrt(this.x * this.x + this.y * this.y);
    }
  }
});

console.log(o.r);

//до 7 минуты дальше пару еще пару методов
