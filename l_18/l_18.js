var greet = function(greeting) {
    return greeting + "! Мое имя " + this.name;
}

var person = {
  name: "Eclum",
  greet: greet
}

var anotherPerson = {
  name: "John",
  greet: greet
}
console.log(person.greet("Как тебя зовут? Имя"));
console.log(anotherPerson.greet.call(person, "Привет"));
console.log(anotherPerson.greet.apply(person, ["Привет"]));

var bound = greet.bind(anotherPerson);
console.log(bound("Hello"));
